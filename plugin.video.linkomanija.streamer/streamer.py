# -*- coding: utf-8 -*-
import urllib
import urllib2
import sys
import pprint
import gzip

from bs4 import BeautifulSoup
from StringIO import StringIO
reload(sys)
sys.setdefaultencoding('utf8')
from transliterate import translit, get_available_language_codes

import httplib

class LinkomanijaStreamer(object):
    def __init__(self):
        self.USERNAME = ''
        self.PASSWORD = ''
        self.COOKIE = ''
        self.HOST = 'www.linkomanija.net'
        self.HTTP = None
        self.HTTPS = None

        self.CATEGORIES = []
        
        self.setCookie()

    def getHTTP(self):
        if not self.HTTP:
            self.HTTP = httplib.HTTPConnection(self.HOST)
        return self.HTTP

    def getHTTPS(self):
        if not self.HTTPS:
            self.HTTPS = httplib.HTTPSConnection(self.HOST)
        return self.HTTPS

    def setCredential(self, username, password):
        self.USERNAME = username
        self.PASSWORD = password

    def unzipResponse(self, response):
        if response.getheader('Content-Encoding') == 'gzip':
            buf = StringIO(response.read())
            f = gzip.GzipFile(fileobj=buf)
            return f.read()
        else:
            return response.read()

    def setCookie(self):
        c = self.getHTTPS()
        c.request('GET', '/')
        response = c.getresponse()
        
        response.read()
        self.COOKIE = response.getheader('set-cookie').replace('; path=/', '')
    
    def getContent(self, url = '/login.php', headers = {}):
        headers['Cookie'] = self.COOKIE

        c = self.getHTTPS()
        c.request('GET', url, headers = headers)
        response = c.getresponse()
        fdata = self.unzipResponse(response)
        soup = BeautifulSoup(fdata, 'html.parser')
        return soup

    def login(self):
        loginData = {}
        loginData['username'] = self.USERNAME
        loginData['password'] = self.PASSWORD
        loginData['commit'] = 'Prisijungti'
        loginData['login_cookie'] = '1'
        loginData = urllib.urlencode(loginData)
        c = self.getHTTPS()
        c.request('POST', '/takelogin.php', loginData, headers = {'Cookie' : self.COOKIE, 'Accept-encoding' : 'gzip, deflate, br', 'Content-Type' : 'application/x-www-form-urlencoded'})
        response = c.getresponse()
        response.read()

    def parseCategories(self):
        categories = []
        names = ['TV', 'Movies']
        content = self.getContent('/browse.php')
        categoryElements = content.find_all('a', class_='catlink')
        for category in categoryElements:
            confirm = False
            for name in names:
                if category.string.find(name) != -1:
                    url = category.get('href')
                    c = {}
                    c['title'] = category.string
                    c['url'] = url
                    c['image'] = self.getCategoryImage(url)
                    categories.append(c)
        self.getCategoryContent(categories[0]['url'])

        self.CATEGORIES = categories
        return categories

    def getCategoryImage(self, categoryURL):
        page = self.getContent(categoryURL)
        imgUrl = page.find_all('a', attrs={'href': categoryURL.replace('/','')})[0].img.get('src')
        return imgUrl

    def getCategoryContent(self, categoryURL):
        content = []
        page = self.getContent(categoryURL)
        tables = page.find_all('table')
        torrentTable = None
        for table in tables:
            if table.get('border') == '1':
                torrentTable = table

        contentRows = torrentTable.find_all('tr');
        
        for row in contentRows[1:]:
            item = {}
            column = row.find_all('td')
            item['freeleech'] = ''
            for link in column[1].find_all('a'):
                if link.get('href') == 'faq.php#stat9':
                    item['freeleech'] = '! '
            item['details_url'] = '/' + column[1].find('a').get('href')
            item['torrent_url'] = '/' + column[1].find('a', class_='index').get('href')
            item['title'] = translit(item['freeleech'] + row.find('b').string, 'ru', reversed=True)
            item['size'] = column[5].contents[0] + column[5].contents[2]
            item['downloads'] = column[6].string
            item['seeders'] = column[7].find('span').string
            if column[8].string == '0':
                item['leechers'] = column[8].string
            else:
                item['leechers'] = column[8].find('a').string
            dateContents = column[4].find('nobr').contents
            dateContents.append('o')
            item['date'] = dateContents[0] + ' ' + dateContents[2]
            item['image'] = self.getMovieImage(item['details_url'])
            content.append(item);
        return content

    def getMovieDescription(self, url):
        page = self.getContent(url)
        return page.find('td', class_='descr_text')
    
    def getMovieImage(self, url):
        d = self.getMovieDescription(url)
        img = d.find('img')
        if img != None:
            return img.get('src')
        else:
            return ''
        
    
streamer = LinkomanijaStreamer()

streamer.setCredential('lunatictm', 'asilasa')
streamer.login()
cats = streamer.parseCategories()
